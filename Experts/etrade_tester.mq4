//+------------------------------------------------------------------+
//|                                                etrade_tester.mq4 |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property version   "1.1"
#property strict

#include "../Include/etrade.mqh"

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   Comment("12345");

   doOnInit();
   return INIT_SUCCEEDED;
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit( const int reason )
{
   doOnDeinit();
}

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
   doOnTimer();
}
//+------------------------------------------------------------------+
