#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"

#import "Kernel32.dll"
   bool QueryPerformanceCounter(long &qp);
	bool QueryPerformanceFrequency(long &qf);
#import

long qf_ = 0;
long qp_ = 0;

long tickOffset_ = 0;

int CLIENT_ID = 0;
void initId(){
  string pathInfo[];
  int n = StringSplit(
   TerminalInfoString(TERMINAL_PATH),
   '\\',
   pathInfo
   );
  CLIENT_ID = StringToInteger(pathInfo[n-1]);
}
void initNano(){
   QueryPerformanceFrequency(qf_);
}
long nanoTime(){
   QueryPerformanceCounter(qp_);
   return ((double)qp_ / qf_) * 1000000000;
}
long getUID(){
   long tick = round((double)nanoTime() / 1000000);
   return TimeLocal() * 1000 + (tick - MathRound((double)tick / 1000) * 1000);
}
enum events {
      EVENT_STAGE,
      EVENT_TRADE,
      EVENT_INIT,
      EVENT_ORDER,
      EVENT_DEAL,
      EVENT_ACTIVE_TRADE,
      EVENT_LOG,
      EVENT_LOAD_RATES,
      EVENT_RATES_LOADED,
      EVENT_RATES_TICK
   };
enum commands {
      CMD_SET_EVENT = 0x01,
      CMD_SET_ACTIVE = 0x04,
      CMD_CLOSE_ORDER = 0x05,
      CMD_CLOSE_ORDER_BY_PRICE = 0x09,
      CMD_PING = 0x11,
      CMD_SET_ORDER = 0x12,
      CMD_FRAME_START = 0x13,
      CMD_FRAME_STOP = 0x14
   };
   
//const int REFRESH_ORDER_MAGIC = 12345;
//const int TRADE_ORDER_MAGIC = 1234;

const int TIMER_CLOSE = 1;
const int TIMER_CLOSE_TP = 2;

const int MASK_ASK = 1;
const int MASK_BID = 1 << 1;
const int MASK_BALANCE = 1 << 2;
const int MASK_CURRENCY = 1 << 3;
const int MASK_MAXLOT = 1 << 4;
const int MASK_DIGITS = 1 << 5;

const int ORDER_MASK_ST = 1;
const int ORDER_MASK_TP = 1 << 1;

const int TYPE_RESPONSE = 0;
const int TYPE_PUSH = 1;
const int TYPE_LOG = 2;

const int PUSH_MSG_TYPE_LOGON = 1;
const int PUSH_MSG_TYPE_PING = 4;
const int PUSH_MSG_TYPE_MARKET_DATA = 2;
const int PUSH_MSG_TYPE_REFRESH_ORDER = 5;
const int PUSH_MSG_TYPE_ADD_ORDER = 7;
const int PUSH_MSG_TYPE_UPDATE_ORDER = 8;
const int PUSH_MSG_TYPE_ADD_DEAL = 9;
const int PUSH_MSG_TYPE_RATES = 10;
const int PUSH_MSG_TYPE_RATES_TICK = 11;

const int LOG_UPDATE_CONNECT = 1;
const int LOG_WAITING_SIGNAL = 2;
const int LOG_ON_SIGNAL_PRICE_NOT_GOOD = 3;
const int LOG_ERROR_OPEN_ORDER = 4;
const int LOG_ERROR_MODIFY_ORDER = 5;

const int MSG_TYPE_STAGE_DATA = 1;

int getMult(string symbol){
    int mult = 1;
    int digits = SymbolInfoInteger(symbol, SYMBOL_DIGITS);
    if (digits==3 || digits==5){mult=10;}
    return mult;
}