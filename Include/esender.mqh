//+------------------------------------------------------------------+
//|                                                      esender.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "epacket.mqh"

class ESender
{
public:
   ESender( int capacity );

   void writeLog( int type );
   void writeLog( int type, long tick, string data );
  
   int frameId;
   EPacket packet;
};

ESender::ESender( int capacity ) :
   packet( capacity )
{
}

void ESender::writeLog( int type )
{
   writeLog( type, nanoTime(), "" );
}

void ESender::writeLog( int type, long tick, string data )
{
   packet.begin( EVENT_LOG );
   packet.writeInteger( frameId );
   packet.writeWord( ( ushort ) type );
   packet.writeLong( tick );
   packet.writeString( data );
   packet.end();
}
