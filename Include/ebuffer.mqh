//+------------------------------------------------------------------+
//|                                                      ebuffer.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "const.mqh"
#include "external.mqh"

struct EBuffer
{
public:
   EBuffer( int size );
   ~EBuffer();

   bool receive( int id, int timeout );
   bool send();

   void reset();
   
   uchar readByte();
   ushort readWord();
   int readInteger();
   long readLong();
   string readString();

   void writeByte( uchar value );
   void writeWord( ushort value );
   void writeInteger( int value );
   void writeLong( long value );
   void writeString( string value );
   
protected:
   int currentIndex_;
   int currentPosition_;
   uchar data_[];
};
  
  
EBuffer::EBuffer( int size )
{
   ArrayResize( data_, size );
   reset();
}

EBuffer::~EBuffer()
{
   ArrayFree( data_ );
}

bool EBuffer::receive( int id, int timeout )
{
   long tick = 0;
   reset();
   return interprocess_slave_recieve_common( id, data_, ArraySize( data_ ), timeout, tick ) != 0;
}


bool EBuffer::send()
{
   if ( currentIndex_ == 0 )
      return false;

   bool result = interprocess_slave_send( data_, currentIndex_ ) == currentIndex_;
   currentIndex_ = 0;
   
   return result;
}

void EBuffer::reset()
{
   currentIndex_ = 0;
   currentPosition_ = 0;
}


uchar EBuffer::readByte()
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 1;
   
   uchar result = data_[ offset ];
	
	return result;
}

ushort EBuffer::readWord()
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 2;
   
	ushort result = data_[ offset ] << 8;
	result |= data_[ offset + 1 ];
	
	return result;
}

int EBuffer::readInteger()
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 4;
   	
	int result = data_[ offset ] << 24;
	result |= data_[ offset + 1 ] << 16;
	result |= data_[ offset + 2 ] << 8;
	result |= data_[ offset + 3 ];
	
	return result;
}

long EBuffer::readLong()
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 8;
   	
	uint high = data_[ offset ] << 24;
	high |= data_[ offset + 1 ] << 16;
	high |= data_[ offset + 2 ] << 8;
	high |= data_[ offset + 3 ];

   uint low = data_[ offset + 4 ] << 24;
	low |= data_[ offset + 5 ] << 16;
	low |= data_[ offset + 6 ] << 8;
	low |= data_[ offset + 7 ];
		
   long result = high;
   result *= 4294967296;
   result += low;

	return result;
}


string EBuffer::readString()
{
   ushort length = readWord();
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += length;
   
   string result = CharArrayToString( data_, offset, length );
   
   return result;
}

void EBuffer::writeByte( uchar value )
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 1;
   
   data_[ offset ] = value;
}

void EBuffer::writeWord( ushort value )
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 2;
   
   data_[ offset ] = value >> 8;
	data_[ offset + 1 ] = value & 0xFF;
}

void EBuffer::writeInteger( int value )
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 4;
   
   data_[ offset ] = value >> 24;
	data_[ offset + 1] = ( value >> 16 ) & 0xFF;
	data_[ offset + 2] = ( value >> 8 ) & 0xFF;
	data_[ offset + 3] = value & 0xFF;
}

void EBuffer::writeLong( long value )
{
   int offset = currentIndex_ + currentPosition_;
   currentPosition_ += 8;
   
   data_[ offset ] = value >> 56;
	data_[ offset + 1] = ( value >> 48 ) & 0xFF;
	data_[ offset + 2] = ( value >> 40 ) & 0xFF;
	data_[ offset + 3] = ( value >> 32 ) & 0xFF;
	data_[ offset + 4] = ( value >> 24 ) & 0xFF;
	data_[ offset + 5] = ( value >> 16 ) & 0xFF;
	data_[ offset + 6] = ( value >> 8 ) & 0xFF;
	data_[ offset + 7] = value & 0xFF;
}

void EBuffer::writeString( string value )
{
   if ( value == "" ) {
       writeWord( 0 );
   } else {
      int offset = currentIndex_ + currentPosition_;
      int length = StringToCharArray( value, data_, offset + 2 ) - 1;
      writeWord( length );
      currentPosition_ += length;
   }
}