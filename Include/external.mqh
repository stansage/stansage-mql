//+------------------------------------------------------------------+
//|                                                     external.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

int interprocess_slave_recieve_common( int event_id, char & p_buffer[], int bc_buffer_capacity, int timeout, long & tick )
{
   Print( __FUNCTION__, __LINE__, ", ", event_id, ", ", bc_buffer_capacity, ", ", timeout );

   if ( GetTickCount() % 3 == 0 )
      return 0;
  
   //Sleep( MathRand() % timeout );
   
   static int type = 0;
   p_buffer[ 0 ] = ( type % 6 ) + 4;
   ++ type;
//   Print( __FUNCTION__, __LINE__, ", ", to_hex( p_buffer ) );

   return 1;
}

int interprocess_slave_recieve( int frame_id, char & p_buffer[], int bc_buffer_capacity )
{
   Print( __FUNCTION__, __LINE__, ", ", frame_id, ", ", bc_buffer_capacity );

   static int test_ = 0;
   if ( ( test_ > 45 ) && ( ChartClose() == true ) )
      return 0;
      
   string name = IntegerToString( test_ ++ )  + ".test";
   int file = FileOpen( name, FILE_READ | FILE_BIN );
   if ( file == INVALID_HANDLE )
      return 0;
      
   return FileReadArray( file, p_buffer );
}

int interprocess_slave_send( char & buffer[], int len )
{
   Print( __FUNCTION__, __LINE__, ", ", len );
   
   if ( len == 0 )
      return 0;

   string name = IntegerToString( GetTickCount() ) + ".send";
   int file = FileOpen( name, FILE_WRITE | FILE_BIN );
   if ( file == INVALID_HANDLE )
      return 0;
   
   return FileWriteArray( file, buffer, 0, len );
}

long sendOrder( string symbol, int type, double qty, double price, int div, double, double, int magic, double & volume, double & openPrice, double & commission )
{
   Print( __FUNCTION__, __LINE__, ", ", symbol, ", ", type, ", ", qty, ", ", price, ", ", div, ", ", magic, ", " );

   volume = 1.5;
   openPrice = 9.9;
   commission = 0.1;
   
   static long id = 1000;

   return id ++;
}

bool closeOrder( int orderId, string symbol, int percent )
{
   Print( __FUNCTION__, __LINE__, ", ", orderId, ", ", symbol, ", ", percent );
   
   return true;
}

bool positionInfo( int orderId, string symbol, int & type, double & price, double & sl, double & tp )
{
   Print( __FUNCTION__, __LINE__, ", ", orderId, ", ", symbol );

   return false;
}

double getMaxLot( string symbol )
{
   Print( __FUNCTION__, __LINE__, ", ", symbol );
   
   return 1.1;
}

string getAccountCurrency()
{
   return "USD";
}

#ifdef __MQL5__

bool RefreshRates()
{
   return true;
}

bool IsTradeContextBusy()
{
   return false;
}

#endif 

/* TESTS:
 * - buffer
 * 10 - CMD_SET_ACTIVE(1)
 * 12 - CMD_SET_EVENT():CMD_SET_ACTIVE(0)
 * 13 - CMD_SET_ACTIVE(1):CMD_SE ...
 * 15 - ... T_EVENT():CMD_SET_ACTIVE(0)
 * - timer
 * 20 - CMD_SET_ACTIVE(1):CMD_SET_EVENT(TIMER(AUTO))
 * 23 - CMD_SET_EVENT(TIMER(CLOSE))
 * 25 - CMD_SET_EVENT(TIMER(CLOSE_TP))
 * 29 - CMD_SET_EVENT(TIMER(IS_TP,IS_SL)):CMD_SET_ACTIVE(0)
 */