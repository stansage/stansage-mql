//+------------------------------------------------------------------+
//|                                                      epacket.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "ebuffer.mqh"

struct EPacket : public EBuffer
{
   EPacket( int size );
   ~EPacket();

   bool receive( int id );
   bool next();
   
   void begin( uchar type );
   void end();
   
protected:
   int nextIndex_;
   int receiveLength_;
   char swap_[];
};

EPacket::EPacket( int size ) : EBuffer( size )
{
   nextIndex_ = 0;
   receiveLength_ = 0;
}

EPacket::~EPacket()
{
   ArrayFree( swap_ );
}


bool EPacket::receive( int id )
{
   reset();
   nextIndex_ = 0;
   receiveLength_ = interprocess_slave_recieve( id, data_, ArraySize( data_ ) );
   
   return receiveLength_ != 0;
}


bool EPacket::next()
{
   /*
    * Restore from swap
    */
   int size = ArraySize( swap_ );
   if ( size > 0 ) {
      char tmp[];
      ArrayResize( tmp, receiveLength_ );
      ArrayCopy( tmp, data_ );
      ArrayCopy( data_, swap_ );
      ArrayCopy( data_, tmp, size );
      ArrayResize( swap_, 0 );
      ArrayFree( tmp );
      receiveLength_ += size;
   }
   
   /*
    * Check the packet bounds
    */
	if ( nextIndex_ + 2 <= receiveLength_ ) {
	   
	   currentIndex_ = nextIndex_;
	   currentPosition_ = 0;
   
      ushort packetLength = readWord();
	   nextIndex_ += packetLength + 2;

	   if ( nextIndex_ <= receiveLength_ )
	      return true;
	      
	   nextIndex_ = currentIndex_; 
   }

   /*
    * Backup to swap
    */
   size = receiveLength_ - nextIndex_;
   if ( size > 0 ) {
      ArrayResize( swap_, size );
      ArrayCopy( swap_, data_, 0, nextIndex_, size );
   }
   
   return false;
}

void EPacket::begin( uchar type )
{
	currentPosition_ = 2;
	writeByte( type );
}

void EPacket::end()
{
   int size = currentPosition_ - 2;

   data_[ currentIndex_ ] = size >> 8;
	data_[ currentIndex_ + 1 ] = size & 0xFF;

	currentIndex_ += currentPosition_;
}



   
