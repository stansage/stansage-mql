//+------------------------------------------------------------------+
//|                                                       eorder.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "esender.mqh"

class EOrder
{
public:
   EOrder( ESender * sender );

   bool isOpen() const;
   bool isNew() const;
   
   void update( EPacket & packet );
   void open( string positionId, int type, double qty, int div, int prolet, double profit );
   void openManual( long id );
   void send();
   
   void closeByHand( bool force, int percent );
   void closeByPercent( int percent );
   void closeByPrice( double price );
   void closeByTimer();
   void supportPos();

protected:
   void initOpen( string positionId, int type, double qty, int div, int prolet, double profit, long startOpenMS );
   void pushUpdate( long orderId, long tick, double st, double tp );
   
private:
   ESender * sender_;
   
   long id_;
   string symbol_;
   ushort symbolMult_;
   int stopLoss_;
   int pointsBU_;
   int normalSpread_;
   int divBU_;
   bool isReverse_;
   int magic_;
   
   bool isNew_;
   bool isBUModifyOk_;
   int type_;
   int div_;
   double openPrice_;
   double tpPrice_;
   double slPrice_;
   
   struct Timer
   {
   public:
      bool trigger;
      long expire;
      uchar auto;
      bool isTP;
      bool isSL;
   } timer_;
};

EOrder::EOrder( ESender * sender ) :
   sender_( sender ),
   id_( 0 ),
   isNew_( false ),
   isBUModifyOk_( true )
{
   timer_.trigger = false;

   initId();
}

bool EOrder::isOpen() const
{
   return id_ > 0;
}

bool EOrder::isNew() const
{
   return isNew_;
}

void EOrder::update( EPacket & data ) 
{
   id_ = 0;
   isNew_ = true;

   symbol_ = data.readString();
   symbolMult_ = data.readWord();
   stopLoss_ = data.readInteger();
   pointsBU_ = data.readInteger() * symbolMult_;
   normalSpread_ = data.readInteger();
   divBU_ = data.readInteger() * symbolMult_;
   isReverse_ = data.readByte() != 0;
   magic_ = data.readInteger();
   
   timer_.expire = data.readInteger();
   timer_.auto = data.readByte();
   timer_.isTP = data.readByte() != 0;
   timer_.isSL = data.readByte() != 0;
   timer_.trigger = false;
}

void EOrder::open( string positionId, int type, double qty, int div, int prolet, double profit )
{
   isNew_ = false;

   long startOpenMS = nanoTime();

   RefreshRates();

   if ( IsTradeContextBusy() == false ) {
      qty *= symbolMult_;
      div *= symbolMult_;
      prolet *= symbolMult_;
      profit *= symbolMult_;

      initOpen( positionId, type, qty, div, prolet, profit, startOpenMS );
      if ( isOpen() == true ) {
         type_ = type;
         div_ = div;
         if ( timer_.expire > 0 ) {
            timer_.expire *= 1000;
            timer_.expire += GetTickCount();
         }
      }
   }
   
   //Print( "pos: ", positionId, ", type = ", type, ", qty = ", qty, ", div = ", div, ", profit = ", profit );
   if ( isOpen() == true ) {
      Print( "Order open success ", id_ );
   } else {
      Print( "Order open fail" );
   }
}

void EOrder::openManual( long id )
{
   id_ = id;
   isNew_ = false;
}

void EOrder::closeByHand( bool force, int percent )
{
   if ( ( force == true ) || ( timer_.trigger == false ) ) { 
      closeByPercent( percent );
   }
}
         
void EOrder::closeByPercent( int percent )
{
   if ( isOpen() == false )
      return;
      
   int i = 15; 
   while ( ( ( i -- ) > 0 ) && ( closeOrder( id_, symbol_, percent ) == false ) ) {
       Sleep( 200 );
   }
   if ( i <= 0 )
      return;
   
   if ( percent > 100 ) {
      Print( "ORDER CLOSED ", id_ );
      id_ = 0;
      isBUModifyOk_ = true;
   }
  
}

void EOrder::closeByPrice( double price )
{
   if ( isOpen() == false )
      return;
      
   if ( price < 0.0001 ) {
      price = SymbolInfoDouble( symbol_, type_ == OP_BUY ? SYMBOL_BID : SYMBOL_ASK );
      Print( "CLOSE BY PRICE SET ", price );
   }
   
   bool case1 = type_ == OP_BUY && openPrice_ <= price;
   bool case2 = type_ == OP_SELL && openPrice_ >= price;
   if ( ( case1 == true ) || ( case2 == true ) ) {
      Print( "CLOSE BY PRICE CLOSE ORDER" );
      closeByPercent( 100 );
   } else {
      double sl = 0;
      double tp = 0;
      double p = 0;
      int t = 0;
      positionInfo( id_, symbol_, t, p, sl, tp );
      tp = openPrice_;
      //modifyOrder(tradeOrderId_, symbol_, 0, p, sl, tp);
      pushUpdate( id_, nanoTime(), sl, tp );
      Print( "CLOSE BY PRICE SET TP ", tp );
   }
}

void EOrder::closeByTimer()
{
   /* init.mqh:153 doOnTimer */
   if ( isOpen() == false )
      return;

   if ( timer_.expire > GetTickCount() ) {
      timer_.trigger = true;
   } else {
      if ( timer_.trigger == true ) {
         if ( timer_.auto == TIMER_CLOSE ) {
            closeByPercent( 100 );
         } else if ( timer_.auto == TIMER_CLOSE_TP ) {
            closeByPrice( 0 );
         }
      }
      timer_.trigger = false;
   }
   
   /* init.mqh:635 checkTP */
   if ( isOpen() == false )
      return;

   double ask = SymbolInfoDouble( symbol_, SYMBOL_ASK );
   double bid = SymbolInfoDouble( symbol_, SYMBOL_BID );
   if ( (
         ( tpPrice_ > 0 )
         &&
         ( ( ( type_ == OP_BUY ) && ( bid >= tpPrice_ ) ) || ( ( type_ == OP_SELL ) && ( ask <= tpPrice_ ) ) )
         &&
         ( ! timer_.trigger || timer_.isTP )
      ) || (
         ( slPrice_ > 0 )
         && 
         ( MathAbs( ask - bid ) < div_ * 0.8 )
         &&
         ( ( ( type_ == OP_BUY ) && ( bid <= slPrice_ ) ) || ( ( type_ == OP_SELL ) && ( ask >= slPrice_ ) ) )
         &&
         ( ! timer_.trigger || timer_.isSL )
      )
   ) {
      closeByPercent( 100 );
   }
}

void EOrder::supportPos()
{
   /* init.mqh:672 supportPos */
   if ( ( isBUModifyOk_ == true ) || ( isOpen() == false ) )
      return;
   
   int mult = getMult( symbol_ );
  
   int type;
   double sl;
   double price;
   double tp;
   
   if ( ( positionInfo( id_, symbol_, type, price, sl, tp ) == false ) || ( divBU_ <= 0 ) )
      return;
   
   double opt = -1;
   double point = SymbolInfoDouble( symbol_, SYMBOL_POINT );
   double ask = SymbolInfoDouble( symbol_, SYMBOL_ASK );
   double bid = SymbolInfoDouble( symbol_, SYMBOL_BID );
      
   MqlRates rates[];
   CopyRates( symbol_, PERIOD_M1, 0, 1, rates );
   
   if ( ( type == OP_BUY ) && ( sl < price ) ) {
      if (
         ( price + divBU_ * point * mult < bid )
         ||
         ( price + divBU_ * point * mult < rates[ 0 ].high )
      ) {
         opt = price + pointsBU_ * mult * point;
      }
   } else if( ( type == OP_SELL ) && ( sl > price ) ) {
      if (
         ( price - divBU_ * point * mult > ask )
         ||
         ( price - divBU_ * point * mult > rates[ 0 ].low + ( ask - bid ) )
      ) {
         opt = price - pointsBU_ * mult * point;
      }
   }
   
   if ( opt > 0 ) {
      int startCount = nanoTime();
      slPrice_ = opt;
      pushUpdate( id_, nanoTime(), sl, tp );
      
      /*
      if(modifyOrder(tradeOrderId_, symbol_, type, price, opt, tp)) {
         
         pushOrderUpdate(tradeOrderId_, nanoTime(), opt, tp);
         
         int digits = SymbolInfoInteger(symbol_, SYMBOL_DIGITS);
         
         //log("Modify order success", IntegerToString(GetTickCount()-startCount) + "|" + tradeOrderId_+"|"+DoubleToStr(price,digits)+"|"+
         //     DoubleToStr(opt,digits)+"|"+DoubleToStr(tp,digits));
              
         //Print("Modify success ", GetTickCount()-startCount," ms");
         //Print("In BU "+tradeOrderId_+" "+DoubleToStr(price,digits)+" "+
         //     DoubleToStr(opt,digits)+" "+DoubleToStr(tp,digits));
      } else {
         //Print("Error modify order", ""+IntegerToString(tradeOrderId_)+" |"+DoubleToStr(price)+"|"+
         //     DoubleToStr(opt)+"|"+DoubleToStr(tp));
      } 
      */
   
      isBUModifyOk_ = true;
   }
}

void EOrder::initOpen( string positionId, int type, double qty, int div, int prolet, double profit, long startOpenMS )
{
   id_ = 0;
   if ( ( qty < 0.01 ) || ( symbol_ == "" ) )
      return;

   int mult = getMult( symbol_ );
   int digits = SymbolInfoInteger( symbol_, SYMBOL_DIGITS );
   double ask = SymbolInfoDouble( symbol_, SYMBOL_ASK );
   double bid = SymbolInfoDouble( symbol_, SYMBOL_BID );
   double price = NormalizeDouble( type == OP_BUY ? ask : bid, digits );
   
   MqlRates rates[];
   CopyRates( symbol_, PERIOD_M1, 1, 1, rates );
   
   double close = rates[0].close;
   
   double point = SymbolInfoDouble( symbol_, SYMBOL_POINT );
   bool is1 = ( type == OP_BUY ) && ( price - ( close + normalSpread_ * point * mult ) > prolet * point * mult );
   bool is2 = ( type == OP_SELL ) && ( close - price > prolet * point * mult );
   if ( ( prolet > 0 ) && ( ( is1 == true ) || ( is2 == true ) ) ) {
      string data = price  + "|" + close + "|" + digits;
      sender_.writeLog( LOG_ON_SIGNAL_PRICE_NOT_GOOD, startOpenMS, data );
      return;
   }
   
   long startTime = TimeLocal();
   double volume = 0;
   double commission = 0;
   double openPrice = 0;
   
   id_ = sendOrder( symbol_, type, qty, price , div * mult, 0.0, 0.0, magic_, volume, openPrice, commission );
   
   //Print("SENDED ORDER ID ", tradeOrderId_);
   long openMS = nanoTime();
   bool ok = false;

   if ( isOpen() == false ) {
      string data = symbol_ + "|" + type + "|" + qty + "|" + price + "|" + div + "|" + digits + "|" + GetLastError();
      sender_.writeLog( LOG_ERROR_OPEN_ORDER, startOpenMS, data );
      //Print("Error open trade order ", GetLastError());
   } else if ( openPrice > 0 ) {
      openPrice_ = openPrice;

      ok = true;//positionInfo(tradeOrderId_, symbol_, orderType, openPrice, _SL, _TP);
      if ( !ok ) {
         //log("Error select order", tradeOrderId_);
         Print( "Error select trade order ", id_ );
         return;
      }
     
      double tp = 0.0;
   
      if ( profit > 0 ) {
         tp = profit * point * mult;
         double proletPr = type == OP_BUY ? openPrice - price  : price - openPrice; 
         if ( proletPr > tp * 0.7 ) {
            tp = 0.3 * tp;
         } else if ( proletPr > tp * 0.3 ) {
            tp = 0.7 * tp;
         }
      }

      if ( type == OP_BUY ) {
         tpPrice_ = tp > 0 ? openPrice + tp : 0;
         slPrice_ = stopLoss_ > 0 ? openPrice - stopLoss_ * point * mult : 0;
      } else {
         tpPrice_ = tp > 0 ? openPrice - tp : 0;
         slPrice_ = stopLoss_ > 0 ? openPrice + stopLoss_ * point * mult : 0;
      }
   }
   
   if ( isOpen() == true ) {
      sender_.packet.begin( EVENT_ORDER );
      sender_.packet.writeString( positionId );
      sender_.packet.writeInteger( getUID() );
      sender_.packet.writeLong( startOpenMS );
      sender_.packet.writeLong( startTime );
      sender_.packet.writeString( symbol_ );
      sender_.packet.writeLong( price * 1000000 );
      sender_.packet.writeByte( type );
      sender_.packet.writeInteger( qty * 100 );
      sender_.packet.writeLong( slPrice_ * 1000000 );
      sender_.packet.writeLong( tpPrice_ * 1000000 );
      sender_.packet.writeLong( ( double ) div * ( double ) mult * point * 1000000 );
      sender_.packet.end();
      
      //Print("DIV - ", (double)div * (double)mult * point);
      if ( openPrice <= 0 ) {
         Print( "OPEN PRICE ", openPrice );
      } else {
         sender_.packet.begin( EVENT_DEAL );
         sender_.packet.writeString( positionId );
         sender_.packet.writeInteger( id_ );
         sender_.packet.writeLong( openMS );
         sender_.packet.writeLong( TimeCurrent() );
         sender_.packet.writeByte( type );
         sender_.packet.writeInteger( ( int ) volume * 100 );
         sender_.packet.writeLong( ( long ) openPrice * 1000000 );
         sender_.packet.writeInteger( ( int ) SymbolInfoDouble( symbol_, SYMBOL_TRADE_CONTRACT_SIZE ) );
         sender_.packet.writeInteger( ( int ) commission * 100 );
         sender_.packet.end();
      }
   }
}

void EOrder::pushUpdate( long orderId, long tick, double sl, double tp )
{
   if ( orderId == id_ ) {
      tpPrice_ = tp;
      slPrice_ = sl;
   }
      
   sender_.packet.begin( TYPE_PUSH );
   sender_.packet.writeInteger( CLIENT_ID );
   sender_.packet.writeByte( PUSH_MSG_TYPE_UPDATE_ORDER );
   sender_.packet.writeLong( id_ );
   sender_.packet.writeLong( nanoTime() + tickOffset_ );
   sender_.packet.writeWord( ORDER_MASK_ST | ORDER_MASK_TP );
   sender_.packet.writeLong( sl / symbolMult_ * 100000 );
   sender_.packet.writeLong( tp / symbolMult_ * 100000 );
   sender_.packet.end();  
}

