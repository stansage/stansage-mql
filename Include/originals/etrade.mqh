//+------------------------------------------------------------------+
//|                                                       etrade.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"

long workerChartId_ = 0;

// Stage
int stageTime_ = 0;
string symbol_ = "";
int stageId_ = 0;
double qtyNorm_ = 0;
int divNorm_ = 0;
int profitNorm_ = 0;
double qtyAgr_ = 0;
int divAgr_ = 0;
int profitAgr_ = 0;
double qtyTest_ = 0;
int divTest_ = 0;
int profitTest_ = 0;
int stopLoss_ = 0;
int pointsBU_ = 0;
int normalSpread_ = 0;
int divBU_ = 0;
bool isReverse_ = false;
int magic_ = 0;

// Trade
int tradeOrderId_ = 0;
bool isActiveTrade_ = false;
long sleepTime_ = 0;
double _TP = 0, _SL = 0;
bool modifyLate_ = false;
double openPrice_ = 0;
int type_ = 0;

void doOnInit(){
   initId();
   initNano();
   
   //Print("Event Trade Started CHART ID - ", ChartID());
   
   EventSetMillisecondTimer(50);
}
void doOnTimer(){
    /*
    if(modifyLate_ && (_SL > 0 || _TP > 0)){
      long startmodyMS = nanoTime();
      bool okSetLimit = modifyOrder(tradeOrderId_, symbol_, type_, openPrice_, _SL, _TP);
      if(!okSetLimit) { 
         log(LOG_ERROR_MODIFY_ORDER, startmodyMS, tradeOrderId_ + "|" + openPrice_ + "|" + _SL + "|" + _TP + "|" + SymbolInfoInteger(symbol_, SYMBOL_DIGITS) + "|" + GetLastError());
      }
      modifyLate_ = false;
    }
    */
    if(sleepTime_ > GetTickCount() || !isActiveTrade_) return;

    if(symbol_ != ""){
      MqlRates rates[];
        CopyRates(
         symbol_,
         PERIOD_M1,
         1,
         1,
         rates
        );
    }

    if(tradeOrderId_ > 0){
         tradeOrderId_ = 0;
    }

    if(tradeOnEvent()) {
         sendToChart(workerChartId_, EVENT_TRADE, tradeOrderId_);
    }
}

bool tradeOnEvent() {

    //long timelocal = TimeLocal();
    //if(timelocal > stageTime_ + 300 || timelocal < stageTime_ - 300) return false;
    
    uchar buf[1];
    bool ok = false;
    Print("Wait for event...");
    //log(LOG_WAITING_SIGNAL);
    
    string positionId = "s" + stageId_;
    long tick = 0;
    int len = interprocess_slave_recieve_common(buf, ArraySize(buf), 15000, tick);
    if(len > 0) {
        switch (buf[0]) {
            case 9: // buyNormal
                ok = openOrder(positionId, isReverse_ ? OP_SELL : OP_BUY, qtyNorm_, divNorm_, divNorm_, profitNorm_, stopLoss_);
                break;
            case 8: // sellNorm
                ok = openOrder(positionId, isReverse_ ? OP_BUY : OP_SELL, qtyNorm_, divNorm_, divNorm_, profitNorm_, stopLoss_);
                break;
            case 7: // buyAgr
                ok = openOrder(positionId, isReverse_ ? OP_SELL : OP_BUY, qtyAgr_, divAgr_, divAgr_, profitAgr_, stopLoss_);
                break;
            case 6: // sellAgr
                ok = openOrder(positionId, isReverse_ ? OP_BUY : OP_SELL, qtyAgr_, divAgr_, divAgr_, profitAgr_, stopLoss_);
                break;
            case 5: // buyTest
                ok = openOrder(positionId, isReverse_ ? OP_SELL : OP_BUY, qtyTest_, divTest_, divTest_, profitTest_, stopLoss_);
                break;
            case 4: // sellTest
                ok = openOrder(positionId, isReverse_ ? OP_BUY : OP_SELL, qtyTest_, divTest_, divTest_, profitTest_, stopLoss_);
                break;
        }
        sleepTime_ = GetTickCount() + 15 * 60000;
	     Print("Receive event ", buf[0]);
    } else {
        //Print("No event");
        //log("No event");
    }
    return ok;
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+

void doOnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  { 
      
      if(id < CHARTEVENT_CUSTOM) return;
      
      uchar data[];
      int eventId;
      if(!chartOnEvent(id, eventId, lparam, sparam, data)) return;
      
      switch(eventId) {
         case EVENT_INIT: 
            workerChartId_ = lparam;
         break;
         case EVENT_STAGE:
            updateStage(data);
         break;
         case EVENT_ACTIVE_TRADE: 
            isActiveTrade_ = lparam > 0 ? true : false;
            Print("SET ACTIVE TRADE ", isActiveTrade_);
         break;
      }
  }
  
void updateStage(uchar &buf[]){

   Print("UPDATE STAGE");

   readPacket();
   readByte(buf);
   stageId_ = readInt(buf);
   stageTime_ = readInt(buf);
                 
	string symbol = readString(buf, readShort(buf));
	symbolMult_ = readShort(buf);	         
	stopLoss_ = readInt(buf) * symbolMult_;
	pointsBU_ = readInt(buf) * symbolMult_;
	normalSpread_ = readInt(buf) * symbolMult_;
	divBU_ = readInt(buf) * symbolMult_;
	isReverse_ = readByte(buf) > 0 ? true : false;
	magic_ = readInt(buf);
		         
	int countItems = readByte(buf);
	int itemType;
	double itemQty;
	int itemDiv;
	int itemProfit;
		         
	SymbolSelect( symbol_, false);
	SymbolSelect( symbol, true);
   Print("SYMBOL SET ", symbol);
	//ChartSetSymbolPeriod(ChartID(), symbol, ChartPeriod());
	   
	symbol_ = symbol;		      
		         
	Print("UPDATE STAGE - id: ",stageId_, " time: ", stageTime_,", symbol: ", symbol, ", SL: ", stopLoss_,", NS: ",normalSpread_,", DBU: ", divBU_,", PBU: ", pointsBU_,", items: " + countItems, ", reverse: ", isReverse_, ", magic: ", magic_);
	for(int i=0;i<countItems;i++){
	   itemType = readByte(buf);
		itemQty = (double)readInt(buf) / 1000;
		itemDiv = readInt(buf) * symbolMult_;
      itemProfit = readInt(buf) * symbolMult_;
                    
		Print("UPDATE STAGE - type: ", itemType,", qty: ", itemQty, ", div: " + itemDiv, " itemProfit: ", itemProfit);
                    
		switch(itemType) {
		   case 1:
		      qtyNorm_ = itemQty;
		      divNorm_ = itemDiv;
            profitNorm_ = itemProfit;
		   break;
		   case 2: 
		      qtyAgr_ = itemQty;
		      divAgr_ = itemDiv;
            profitAgr_ = itemProfit;
		   break;
		   case 4:
		      qtyTest_ = itemQty;
		      divTest_ = itemDiv;
            profitTest_ = itemProfit;
		   break;
	   }
   }
}

void initOpenOrder(string positionId, int type, double qty, int div, int prolet, double profit, int stopLoss, long startOpenMS){
    
    if(qty < 0.01) return;
    
    double prOpen;
    
    int mult = getMult(symbol_);
    double ask = SymbolInfoDouble(symbol_, SYMBOL_ASK);
    double bid = SymbolInfoDouble(symbol_, SYMBOL_BID);
    if(type == OP_BUY){
        prOpen = NormalizeDouble(ask, SymbolInfoInteger(symbol_, SYMBOL_DIGITS));
    } else {
        prOpen=NormalizeDouble(bid,SymbolInfoInteger(symbol_, SYMBOL_DIGITS));
    }
    
    MqlRates rates[];
     CopyRates(
      symbol_,
      PERIOD_M1,
      1,
      1,
      rates
     );
    
    double close = rates[0].close;
    
    double point = SymbolInfoDouble(symbol_, SYMBOL_POINT);
    if(
        prolet > 0
        &&
        (
        (type==OP_BUY && (prOpen-(close+normalSpread_*point*mult))>prolet*point*mult)
        ||
        (type==OP_SELL && (close-prOpen)>prolet*point*mult)
        )
    ) {
        log(LOG_ON_SIGNAL_PRICE_NOT_GOOD, startOpenMS, prOpen + "|" + close + "|" + SymbolInfoInteger(symbol_, SYMBOL_DIGITS));
        
        //Print(prOpen," ",close," ",prOpen-close," "," price not good ",MathAbs(close-prOpen)/point/mult," div=",prolet," points");
        
        return;
    }
    
    long startTime = TimeLocal();
    double volume = 0;
    double commission = 0;
    openPrice_ = 0;
    tradeOrderId_ = sendOrder(symbol_, type, qty, prOpen, div*mult, 0.0, 0.0, magic_, volume, openPrice_, commission);
    //Print("SENDED ORDER ID ", tradeOrderId_);
    long openMS = nanoTime();
    
    bool ok;
    
    if(tradeOrderId_ < 0){
        log(LOG_ERROR_OPEN_ORDER, startOpenMS, symbol_ + "|" + type + "|" + qty + "|" + prOpen + "|" + div + "|" + SymbolInfoInteger(symbol_, SYMBOL_DIGITS) + "|" + GetLastError());
        //Print("Error open trade order ", GetLastError());
    } else if (openPrice_ > 0) {
    
        ok = true;//positionInfo(tradeOrderId_, symbol_, orderType, openPrice, _SL, _TP);
        if(!ok) {
            //log("Error select order", tradeOrderId_);
            Print("Error select trade order ", tradeOrderId_);
            return;
        }
        
        
        double tp = 0;
        
        if(profit > 0) {
           tp = profit*point*mult;
           double proletPr = type == OP_BUY ? openPrice_ - prOpen : prOpen - openPrice_; 
           if(proletPr > tp * 0.7){
               tp = 0.3 * tp;
           } else if (proletPr > tp * 0.3) {
               tp = 0.7 * tp;
           }
        }
        
        if(type == OP_BUY){
            _TP=tp > 0 ? openPrice_ + tp : 0;
            _SL= stopLoss > 0 ? openPrice_ - stopLoss*point*mult : 0;
        } else {
            _TP= tp > 0 ? openPrice_ - tp : 0;
            _SL= stopLoss > 0 ? openPrice_ + stopLoss*point*mult : 0;
        }
        
        type_ =  type;
        modifyLate_ = true;
        /*
        okSetLimit = modifyOrder(tradeOrderId_, symbol_, type, openPrice, _SL, _TP);
        if(!okSetLimit) { 
            log(LOG_ERROR_MODIFY_ORDER, startmodyMS, tradeOrderId_ + "|" + openPrice + "|" + _SL + "|" + _TP + "|" + SymbolInfoInteger(symbol_, SYMBOL_DIGITS) + "|" + GetLastError());
            //Print("Error modify opened trade order ", tradeOrderId_);
        }
        */
        //long modyMS=nanoTime();
        
        //int digits = SymbolInfoInteger(symbol_, SYMBOL_DIGITS);
        
        //log("Open order", type + "|" + DoubleToStr(prOpen, digits) + "|" + DoubleToStr(OrderOpenPrice(),digits) + "|" + DoubleToStr(_SL,digits) + "|" + DoubleToStr(_TP,digits) + "|" + qty + "|" + (openMS-eventTime_) + "|" + (modyMS-startmodyMS));
        
        //Print("cmd=",type," want price =",DoubleToStr(prOpen, digits)," open price =",DoubleToStr(OrderOpenPrice(),digits)," stop loss =",DoubleToStr(_SL,digits)," take profit=",DoubleToStr(_TP,digits)," lot=", qty);
        //Print("time to open=",openMS-eventTime_," time to modify=",modyMS-startmodyMS, " event time=", eventTime_);
        
        
    }
    
    if(tradeOrderId_ > 0) {
       Print("PUSH ORDER INFO TO CHART ", workerChartId_);
       startPacket();
       writeString(positionId);
       writeInt(tradeOrderId_ > 0 ? tradeOrderId_ : getUID());
       writeLong(startOpenMS);
       writeLong(startTime);
       writeString(symbol_);
       writeLong(prOpen * 1000000);
       writeByte(type);
       writeInt(qty * 100);
       writeLong(_SL * 1000000);
       writeLong(_TP * 1000000);
       writeLong((double)div * (double)mult * point * 1000000);
       sendToChart(workerChartId_, EVENT_ORDER, endWritePacket());
       
       //Print("DIV - ", (double)div * (double)mult * point);
       if(openPrice_ > 0){
          startPacket();
          writeString(positionId);
          writeInt(tradeOrderId_);
          writeLong(openMS);
          writeLong(TimeCurrent());
          writeByte(type);
          writeInt(volume * 100);
          writeLong(openPrice_ * 1000000);
          writeInt(SymbolInfoDouble(symbol_, SYMBOL_TRADE_CONTRACT_SIZE));
          writeInt(commission * 100);
          sendToChart(workerChartId_, EVENT_DEAL, endWritePacket());
       } else {
         Print("OPEN PRICE ", openPrice_);
       }
    }

}

void log(int type){
   log(type, nanoTime(), "");
}
void log(int type, long tick, string data){
   startPacket();
   writeShort(type);
   writeLong(tick);
   writeString(data);
   sendToChart(workerChartId_, EVENT_LOG, endWritePacket());
}