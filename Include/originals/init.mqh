//+------------------------------------------------------------------+
//|                                                         init.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"

long expire_ = 0;
long expireState_ = 0;
uchar recieve_buf_[1000];
bool initTradeChart_ = false;
long pingTime_ = 0;
int timeout_ = 200;
long timerTick_ = 0;

// Stage
int stageTime_ = 0;
string symbol_ = "";
int stageId_ = 0;
int pointsBU_ = 0;
int divBU_ = 0;
int magic_ = 0;

// Event Trade
long tradeChart_ = 0;
int tradeOrderId_ = 0;
double tradeOrderTPPrice_ = 0;
double tradeOrderSLPrice_ = 0;
double tradeOrderOpenPrice_ = 0;
double orderDiv_ = 0;
int tradeOrderType_ = 0;
bool BUModifyOk_ = true;
int refreshOrderId_ = 0;
long expireRefreshOrder_ = 0;
bool waitForTrade_ = false;
string refreshOrderSymbol_;
bool tradeComplete_ = false;
bool inTimer_ = false;
long timerExpire_ = 0;
bool timerTP_ = false;
bool timerSL_ = false;
uchar timerAuto_ = 0;
int  timer_ = 0;

// State
bool checkMaxLot_ = false;
long ask_ = -1;
long bid_ = -1;
int balance_ = -1;
double maxLot_ = -1;
long pushStateExpire_ = 0;
int digits_ = 0;

// Rates
long ratesId_ = 0;

// Active for trade
bool isActiveTrade_ = false;

// Active RO
bool isActiveRo_ = false;

uchar test_buf[10];

int count = 0;

void log(int type){
   log(type, "");
}
void log(int type, string data){
   log(type, nanoTime(), data);
}
void log(int type, long tick, string data){
    startPacket(CLIENT_ID, TYPE_LOG);
    writeInt(TimeLocal());
    writeLong(tick);
    writeInt(stageId_);
    writeShort(type);
    writeString(data);
    endPacket();
}

void OnTick()
  {
   if(tradeOrderId_ > 0){
      checkTP();
   }
  }

void doOnInit(){

  initId();
  initNano();

  startPacket(CLIENT_ID, TYPE_PUSH);
  writeByte(PUSH_MSG_TYPE_LOGON);
  endPacket();

  string symbols[];
  int totalSymbol = SymbolsTotal(true);
  ArrayResize(symbols, totalSymbol);
  int i;
  for(i=0;i<totalSymbol;i++){
   symbols[i] = SymbolName(i, true);
  }
  for(i=0;i<totalSymbol;i++){
   SymbolSelect(symbols[i], false);
  }
  
  refreshOrderSymbol_ = SymbolName(0, false);
  
  //Print("Refresh order symbol: ", refreshOrderSymbol_);

  MathSrand(GetTickCount());
  
   long currentChartId = ChartID();
   long chartId = ChartFirst();
   string chartName;
   while(chartId >= 0){
      chartName = ChartGetString(chartId, CHART_COMMENT);
      if(chartName == "etrade"){
         tradeChart_ = chartId;
      } else if(chartId != currentChartId){
         ChartClose(chartId);
      }
      chartId = ChartNext(chartId);
   }
   if(tradeChart_ == 0){
      tradeChart_ = ChartOpen(Symbol(), 0);
      ChartApplyTemplate(tradeChart_, "etrade.tpl");
      ChartSetString(tradeChart_, CHART_COMMENT, "etrade");
   }
   
   //activeRates("EURUSD", PERIOD_H1, 0, 20);
   
   EventSetMillisecondTimer(timeout_);
	
   //Print("!!! STARTED !!! CHART ID", ChartID());

}
void doOnTimer(){
   int time = TimeLocal();
   long curTickCount = GetTickCount();
   
   if(!initTradeChart_) {
      Sleep(1000);
      bool ok  = sendToChart(tradeChart_, EVENT_INIT, ChartID());
      //Print("SEND EVENT IS ", ok);
      initTradeChart_ = true;
   }

   if(tradeOrderId_ > 0){
       if(timerExpire_ > curTickCount){
         inTimer_ = true;
       } else {
         if(inTimer_){
            if(timerAuto_ == TIMER_CLOSE) {
               doCloseOrder(100);
            } else if(timerAuto_ == TIMER_CLOSE_TP){
               closeByPrice(0);
            }
         }
         inTimer_ = false;
       }
       supportPos();
   } else {
      if(isActiveTrade_ && isActiveRo_ && refreshOrderSymbol_ != ""){
         refreshOrder(refreshOrderSymbol_, time);
      } else if(refreshOrderId_ > 0){
         closeRefreshOrder();
      }
   }
   //Print("Stage time", stageTime_, " local Time ", TimeLocal());
   //Print("Before stage time - ", stageTime_ - time, ", Active trade - ", isActiveTrade_, ", waitForTrade - ", waitForTrade_);
   uchar type;
	int in_buf_size;
	int last_size;
	uchar in_buf_last[];
	
	ArrayResize(in_buf_, 0, 1000);	

	in_len_ = interprocess_slave_recieve(CLIENT_ID, recieve_buf_, ArraySize(recieve_buf_));
	//Print("Recived ", in_len_, " bytes");
	if(in_len_ > 0){
	   in_buf_next_packet_ind_ = 0;
		in_buf_size = ArraySize(in_buf_);
		ArrayResize(in_buf_, in_len_ + in_buf_size, 1000);
		ArrayCopy(
         in_buf_,             // Ã?ÂºÃ‘Æ’Ã?Â´Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼
         recieve_buf_,         // Ã?Â¾Ã‘â€šÃ?ÂºÃ‘Æ’Ã?Â´Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼
         in_buf_size,         // Ã‘Â? Ã?ÂºÃ?Â°Ã?ÂºÃ?Â¾Ã?Â³Ã?Â¾ Ã?Â¸Ã?Â½Ã?Â´Ã?ÂµÃ?ÂºÃ‘Â?Ã?Â° Ã?Â¿Ã?Â¸Ã‘Ë†Ã?ÂµÃ?Â¼ Ã?Â² Ã?Â¿Ã‘â‚¬Ã?Â¸Ã?ÂµÃ?Â¼Ã?Â½Ã?Â¸Ã?Âº
         0,         // Ã‘Â? Ã?ÂºÃ?Â°Ã?ÂºÃ?Â¾Ã?Â³Ã?Â¾ Ã?Â¸Ã?Â½Ã?Â´Ã?ÂµÃ?ÂºÃ‘Â?Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼ Ã?Â¸Ã?Â· Ã?Â¸Ã‘Â?Ã‘â€šÃ?Â¾Ã‘â€¡Ã?Â½Ã?Â¸Ã?ÂºÃ?Â°
         in_len_    // Ã‘Â?Ã?ÂºÃ?Â¾Ã?Â»Ã‘Å’Ã?ÂºÃ?Â¾ Ã‘Â?Ã?Â»Ã?ÂµÃ?Â¼Ã?ÂµÃ?Â½Ã‘â€šÃ?Â¾Ã?Â²
      );
      in_len_ += in_buf_size;
      
      //Print("Buffer data: ", to_hex(in_buf_));
      
		while (nextPacket()){
		   type = readByte();
		   switch(type){
		        case 11:
		            startPacket(CLIENT_ID, TYPE_PUSH);
                  writeByte(PUSH_MSG_TYPE_PING);
                  writeLong(nanoTime());
                  endPacket();
		            break;
		        case 10:
		            isActiveRo_ = readByte() > 0 ? true : false;
		            if(isActiveRo_){
		               setRefreshOrder(symbol_);
		            }
		            Print("Set active RO: ", isActiveRo_);
		            break;
		        case 9:
		            if(!inTimer_) {
		               closeByPrice((double)readLong() / 1000000);
		            }
		            break;
		        case 8:
		            inactiveRates();
		            break;
		        case 7:
		            doActiveRates();
		            break;
		        case 6:
		            tickOffset_ = readLong();
		            Print("TICK OFFSET - ", tickOffset_, " tick - ", nanoTime());
		            break;
              case 5:
                  doCloseOrder();
                  break;
		      case 4:
		         isActiveTrade_ = readByte() > 0 ? true : false;
		         sendToChart(tradeChart_, EVENT_ACTIVE_TRADE, isActiveTrade_ ? 1 : 0);
		         setRefreshOrder(symbol_);
		         Print("Set active trade: " + isActiveTrade_);
		         break;
		      case 1:  
               stageId_ = readInt();
               stageTime_ = readInt();
               symbol_ = readString(readShort());
               symbolMult_ = readShort();
               readInt();
	            pointsBU_ = readInt() * symbolMult_;
	            readInt();
	            divBU_ = readInt() * symbolMult_;
	            readByte();
	            updateMagic(readInt());
	            timer_ = readInt();
	            timerAuto_ = readByte();
	            timerTP_ = readByte() > 0;
	            timerSL_ = readByte() > 0;
	            
               sendToChart(tradeChart_, EVENT_STAGE, getPacket()); 
               
               initOrders();
               setRefreshOrder(symbol_);
               
		         break;
		   }
            
		}
		
		last_size = in_len_ - in_buf_next_packet_ind_;
		ArrayResize(in_buf_last, last_size, 1000);
		ArrayCopy(
         in_buf_last,             // Ã?ÂºÃ‘Æ’Ã?Â´Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼
         in_buf_,         // Ã?Â¾Ã‘â€šÃ?ÂºÃ‘Æ’Ã?Â´Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼
         0,         // Ã‘Â? Ã?ÂºÃ?Â°Ã?ÂºÃ?Â¾Ã?Â³Ã?Â¾ Ã?Â¸Ã?Â½Ã?Â´Ã?ÂµÃ?ÂºÃ‘Â?Ã?Â° Ã?Â¿Ã?Â¸Ã‘Ë†Ã?ÂµÃ?Â¼ Ã?Â² Ã?Â¿Ã‘â‚¬Ã?Â¸Ã?ÂµÃ?Â¼Ã?Â½Ã?Â¸Ã?Âº
         in_buf_next_packet_ind_,         // Ã‘Â? Ã?ÂºÃ?Â°Ã?ÂºÃ?Â¾Ã?Â³Ã?Â¾ Ã?Â¸Ã?Â½Ã?Â´Ã?ÂµÃ?ÂºÃ‘Â?Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼ Ã?Â¸Ã?Â· Ã?Â¸Ã‘Â?Ã‘â€šÃ?Â¾Ã‘â€¡Ã?Â½Ã?Â¸Ã?ÂºÃ?Â°
         last_size    // Ã‘Â?Ã?ÂºÃ?Â¾Ã?Â»Ã‘Å’Ã?ÂºÃ?Â¾ Ã‘Â?Ã?Â»Ã?ÂµÃ?Â¼Ã?ÂµÃ?Â½Ã‘â€šÃ?Â¾Ã?Â²
      );
      ArrayResize(in_buf_, last_size, 1000);
      ArrayCopy(
         in_buf_,             // Ã?ÂºÃ‘Æ’Ã?Â´Ã?Â° Ã?ÂºÃ?Â¾Ã?Â¿Ã?Â¸Ã‘â‚¬Ã‘Æ’Ã?ÂµÃ?Â¼
         in_buf_last
      );
      
      //Print("Buffer data after read: ", to_hex(in_buf_));
	}
   
   //tickCount = GetTickCount();
   /*
   if(expireState_ < tickCount) {
      expireState_ = tickCount + 500;
      pushState();
   }
   */
  
   pushState();
   /*
   if(expire_ < time) {
      expire_ = time + 2;
      pingTime_ = GetTickCount();
      startPacket(CLIENT_ID, TYPE_PUSH);
      writeByte(PUSH_MSG_TYPE_PING);
      writeLong(expire_ + 2);
      endPacket();
   }
   */
   onEndTimer();
   sendBuffer();
   
   timerTick_ = curTickCount;
}
void updateMagic(int magic){
   if(magic != magic_){
	   closeRefreshOrder();
	}
	magic_ = magic;
}
void setRefreshOrder(string symbol){
   if(refreshOrderId_ > 0 && refreshOrderSymbol_ != symbol) {
	   closeRefreshOrder();
	}
   refreshOrderSymbol_ = symbol;
}
void doCloseOrder(){
   startPacket(CLIENT_ID, TYPE_RESPONSE);
   writeLong(readLong());
   bool force = readByte() > 0;
   int percent = readShort();
   if(((!inTimer_) || force) && doCloseOrder(percent)){
      writeByte(1);
   } else {
      writeByte(0);
   }   
   endPacket();
}
bool doCloseOrder(int percent){
   int i=15; 
   while((i--)>0 && !closeOrder(tradeOrderId_, symbol_, percent)) {
       Sleep(200);
   }
   
   if(i <= 0) return false;
   
   if(percent == 100) {
       tradeOrderId_ = 0;
       BUModifyOk_ = true;
   }
   return true;
}
void doActiveRates(){
      string ratesSymbol = readString(readShort());
      long ratesFrom = round((double)readLong() / 1000);
      int ratesCount = readInt();
      activeRates(ratesSymbol, PERIOD_M1, ratesFrom, ratesCount);
}
void doOnChartEvent (const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam) {
         if(id < CHARTEVENT_CUSTOM) return;
     
      uchar data[];
      int eventId;
      if(!chartOnEvent(id, eventId, lparam, sparam, data)) return;
      
      switch(eventId){
         case EVENT_TRADE: 
            refreshOrderSymbol_ = "";
            tradeOrderId_ = lparam;
            BUModifyOk_ = false;
         break;
         case EVENT_ORDER:
            onEventOrder(data);
         break;
         case EVENT_DEAL:
            onEventDeal(data);
         break;
         case EVENT_LOG:
            onEventLog(data);
         break;
         case EVENT_RATES_LOADED:
            onLoadRates(data, lparam);
         break;
         case EVENT_RATES_TICK:
            onRatesTick(sparam, lparam);
         break;
      }        
}

void onRatesTick(string sparam, int timeframe){
   string symbol = StringSubstr(sparam, 18);
   //Print("Rates TICK ", symbol," - ", timeframe);
   startPacket(CLIENT_ID, TYPE_PUSH);
   writeByte(PUSH_MSG_TYPE_RATES_TICK);
   writeLong(ratesId_);
   
   //Print("ratesId_ ",ratesId_);
   
   MqlRates rates[];
   CopyRates(
      symbol,
      (ENUM_TIMEFRAMES)timeframe,
      0,
      1,
      rates
   );
   //Print("Data ",rates[0].time + ratesDiffTime_, " ", rates[0].low, " ", rates[0].open, " ", rates[0].close, " ", rates[0].high);
   writeLong((rates[0].time + ratesDiffTime_) * 1000);
   writeLong(rates[0].low / symbolMult_ * 1000000);
   writeLong(rates[0].open / symbolMult_ * 1000000);
   writeLong(rates[0].close / symbolMult_ * 1000000);
   writeLong(rates[0].high / symbolMult_ * 1000000);
   endPacket();
}
void onLoadRates(uchar &buf[], int nb){
   
   ratesId_ = GetTickCount();
   
   readPacket();
   string symbol = readString(buf, readShort(buf));
   int timeframe = readShort(buf);
   long from = readLong(buf);
   
   MqlRates rates[];
   if(from > 10000){
      nb = CopyRates(symbol, (ENUM_TIMEFRAMES)timeframe, (datetime)from, nb, rates);
   } else {
      nb = CopyRates(symbol, (ENUM_TIMEFRAMES)timeframe, from, nb, rates);
   }
   
   //Print("Rates loaded ", symbol, " - ", timeframe, ", from: ", from, ", count: ", nb);
   
   startPacket(CLIENT_ID, TYPE_PUSH);
   writeByte(PUSH_MSG_TYPE_RATES);
   writeLong(ratesId_);
   writeShort(timeframe);
   writeString(symbol);
   writeInt(MathMax(0, nb));
   for(int i=0; i<nb; i++){
      //Print("Time ", rates[i].time, ", ", rates[i].close, " - ", rates[i].open);
      writeLong((rates[i].time + ratesDiffTime_) * 1000);
      writeLong(rates[i].low / symbolMult_ * 1000000);
      writeLong(rates[i].open / symbolMult_ * 1000000);
      writeLong(rates[i].close / symbolMult_ * 1000000);
      writeLong(rates[i].high / symbolMult_ * 1000000);
      
   }
   endPacket();
}
void onEventLog(uchar &buf[]){

   readPacket();
   
   int type = readShort(buf);
   long tick = readLong(buf);
   string data = readString(buf, readShort(buf));
   
   log(type, tick, data);
}
void onEventDeal(uchar &buf[]){
   
   readPacket();
   
   string positionId = readString(buf, readShort(buf));
   int orderId = readInt(buf);
   long tick = readLong(buf);
   long time = readLong(buf);
   int type = readByte(buf);
   double qty = (double)readInt(buf) / (double)100;
   double price = (double)readLong(buf) / (double)1000000;
   int lotSize = (double)readInt(buf);
   double commission = (double)readInt(buf) / (double)100;
   
   if(orderId == tradeOrderId_ && type == tradeOrderType_){
      tradeOrderOpenPrice_ = price;
      
      timerExpire_ = GetTickCount() + timer_ * 1000;
   }
   
   Print("ON EVENT DEAL ", to_hex(buf));
   Print("DEAL: ",positionId, " - ", orderId,  " - ", tick,  " - ", time,  " - ", type,  " - ", qty, " - ",  price,  " - ", lotSize);
   pushAddDeal(positionId, orderId, tick, time, type, qty, price, lotSize, commission, getAccountCurrency());
   
}
void closeByPrice(double price){
   Print("CLOSE BY PRICE ", price);
   if(tradeOrderId_ == 0) return;
   if(price < 0.0001) {
      price = tradeOrderType_ == OP_BUY ? SymbolInfoDouble(symbol_, SYMBOL_BID) : SymbolInfoDouble(symbol_, SYMBOL_ASK);
      Print("CLOSE BY PRICE SET ", price);
   }
   
   if(
      (tradeOrderType_ == OP_BUY && tradeOrderOpenPrice_ <= price)
      || 
      (tradeOrderType_ == OP_SELL && tradeOrderOpenPrice_ >= price)
   ) {
      Print("CLOSE BY PRICE CLOSE ORDER");
      doCloseOrder(100);
   } else {
      double sl = 0;
      double tp = 0;
      double p = 0;
      int t = 0;
      positionInfo(tradeOrderId_, symbol_, t, p, sl, tp);
      tp = tradeOrderOpenPrice_;
      //modifyOrder(tradeOrderId_, symbol_, 0, p, sl, tp);
      pushOrderUpdate(tradeOrderId_, nanoTime(), sl, tp);
      Print("CLOSE BY PRICE SET TP ", tp);
   }
}
void onEventOrder(uchar &buf[]){
   
   readPacket();
            
   string positionId = readString(buf, readShort(buf));
   int orderId = readInt(buf);
   
   tradeOrderId_ = orderId;
   inTimer_ = false;
   
   long tick = readLong(buf);
   long time = readLong(buf);
   string symbol = readString(buf, readShort(buf));
   double price = (double)readLong(buf) / (double)1000000;
   int type = readByte(buf);
   tradeOrderType_ = type;
   double qty = (double)readInt(buf) / (double)100;
   double sl = (double)readLong(buf) / (double)1000000;
   double tp = (double)readLong(buf) / (double)1000000;
   tradeOrderTPPrice_ = tp;
   tradeOrderSLPrice_ = sl;
   orderDiv_ = (double)readLong(buf) / (double)1000000;
   
   Print("ON EVENT ORDER ", to_hex(buf));
   Print("ORDER: ",  positionId, " - ", orderId, " - ", tick, " - ", time, " - ", symbol, " - ", "(", price, ")", " - ", type, " - ", qty, " - ", sl, " - ", tp, " - ", orderDiv_);
   
   pushOpenOrder(positionId, orderId, tick, time, symbol, price, type, qty, sl, tp, orderDiv_);

}

void pushOrderUpdate(long orderId, long tick, double st, double tp){
   if(orderId == tradeOrderId_) {
      tradeOrderTPPrice_ = tp;
      tradeOrderSLPrice_ = st;
   }
   startPacket(CLIENT_ID, TYPE_PUSH);
   writeByte(PUSH_MSG_TYPE_UPDATE_ORDER);
   writeLong(orderId);
   writeLong(tick + tickOffset_);
   writeShort(ORDER_MASK_ST | ORDER_MASK_TP);
   writeLong(st / symbolMult_ * 100000);
   writeLong(tp / symbolMult_ * 100000);
   endPacket();
}

void pushState(){

   long time = GetTickCount();

   if(pushStateExpire_ > time) return;
   
   int mask = 0;
   
   checkMaxLot_ = false;
   pushStateExpire_ = time + 500;
   mask = MASK_ASK | MASK_BID | MASK_BALANCE | MASK_CURRENCY | MASK_MAXLOT | MASK_DIGITS;
   
   int vdigits = round(SymbolInfoInteger(symbol_, SYMBOL_DIGITS));
   if(vdigits != digits_){
      digits_ = vdigits;
      mask |= MASK_DIGITS;
   }
   long vbid = round(SymbolInfoDouble(symbol_, SYMBOL_BID) * 100000);
   if(vbid != bid_){
      bid_ = vbid;
      mask |= MASK_BID;
   }
   
   long vask = round(SymbolInfoDouble(symbol_, SYMBOL_ASK) * 100000);
   if(vask != ask_){
      ask_ = vask;
      mask |= MASK_ASK;
   }
   
   int balance = round(AccountInfoDouble(ACCOUNT_BALANCE) * 100);
   if(balance != balance_){
      balance_ = balance;
      mask |= MASK_BALANCE | MASK_CURRENCY;
   }
   
   double _maxLot = maxLot(symbol_);
   if(maxLot_ != _maxLot){
     maxLot_ = _maxLot;
     mask |= MASK_MAXLOT;
   }
   
   if(mask == 0) return;
   
   startPacket(CLIENT_ID, TYPE_PUSH);
   writeByte(PUSH_MSG_TYPE_MARKET_DATA);
   writeInt(TimeLocal());
   writeInt(stageId_);
   writeString(symbol_);
   writeInt(mask);
   if((mask & MASK_DIGITS) > 0) {
      writeByte(digits_);
      writeByte(getMult(symbol_));
   }
   if((mask & MASK_ASK) > 0) writeLong(ask_ / symbolMult_);
   if((mask & MASK_BID) > 0) writeLong(bid_ / symbolMult_);
   if((mask & MASK_BALANCE) > 0) {
      writeInt(balance_);
      writeString(getAccountCurrency());
   }
   if((mask & MASK_MAXLOT) > 0) writeInt(round(maxLot_ * 100));
   endPacket();
   
   //Print("Symbol - ", symbol_," ASK - ", ask_, " BID - ", bid_, " SP - ", ask_ - bid_," balance - ", balance_, " max lot - ", maxLot_);
}

string getEventName(uchar event){
    switch(event){
        case 11:
            return "BN";
        case 33:
            return "SN";
        case 22:
            return "BA";
        case 44:
            return "SA";
        case 55:
            return "BH";
        case 66:
            return "SH";
    }
    return "";
}
void checkTP(){
    double ask = SymbolInfoDouble(symbol_, SYMBOL_ASK);
    double bid = SymbolInfoDouble(symbol_, SYMBOL_BID);
    if (
      (
         tradeOrderTPPrice_ > 0
         &&
         (
         (tradeOrderType_ == OP_BUY && bid >= tradeOrderTPPrice_)
         ||
         (tradeOrderType_ == OP_SELL && ask <= tradeOrderTPPrice_)
         )
         &&
         ((inTimer_ && timerTP_) || !inTimer_)
      ) 
      || 
      (
         tradeOrderSLPrice_ > 0
         && 
         MathAbs(ask-bid) < orderDiv_ * 0.8
         &&
         (
         (tradeOrderType_ == OP_BUY && bid <= tradeOrderSLPrice_)
         ||
         (tradeOrderType_ == OP_SELL && ask >= tradeOrderSLPrice_)
         )
         &&
         ((inTimer_ && timerSL_) || !inTimer_)
      )
   ) {
      doCloseOrder(100);
   }
}
void supportPos(){
    
   checkTP();
   
   if(!BUModifyOk_) {
   
     int mult = getMult(symbol_);
     
     int type;
     double sl;
     double price;
     double tp;
     
     if(!positionInfo(tradeOrderId_, symbol_,  type, price, sl, tp) || divBU_ <= 0) return;
     
     double opt = -1;
     double point = SymbolInfoDouble(symbol_, SYMBOL_POINT);
     
     MqlRates rates[];
     CopyRates(
      symbol_,
      PERIOD_M1,
      0,
      1,
      rates
     );
   
     if(type==OP_BUY && sl<price){
         if( (price + divBU_ * point * mult < SymbolInfoDouble(symbol_, SYMBOL_BID)) || (price + divBU_ * point * mult < rates[0].high) ) {
             opt = price + pointsBU_ * mult * point;
         }
     } else if(type==OP_SELL && sl>price) {
         if(
             (price-divBU_*point*mult>SymbolInfoDouble(symbol_, SYMBOL_ASK)) 
             || 
             (price - divBU_ * point*mult > rates[0].low + (SymbolInfoDouble(symbol_, SYMBOL_ASK) - SymbolInfoDouble(symbol_, SYMBOL_BID)))
         ){
             opt = price - pointsBU_ * mult * point;
         }
     }
     if(opt > 0) {
         int startCount=nanoTime();
         
         tradeOrderSLPrice_ = opt;
         
         pushOrderUpdate(tradeOrderId_, nanoTime(), opt, tp);
         
         /*
         if(modifyOrder(tradeOrderId_, symbol_, type, price, opt, tp)) {
            
            pushOrderUpdate(tradeOrderId_, nanoTime(), opt, tp);
            
            int digits = SymbolInfoInteger(symbol_, SYMBOL_DIGITS);
            
            //log("Modify order success", IntegerToString(GetTickCount()-startCount) + "|" + tradeOrderId_+"|"+DoubleToStr(price,digits)+"|"+
            //     DoubleToStr(opt,digits)+"|"+DoubleToStr(tp,digits));
                 
            //Print("Modify success ", GetTickCount()-startCount," ms");
            //Print("In BU "+tradeOrderId_+" "+DoubleToStr(price,digits)+" "+
            //     DoubleToStr(opt,digits)+" "+DoubleToStr(tp,digits));
         } else {
            //Print("Error modify order", ""+IntegerToString(tradeOrderId_)+" |"+DoubleToStr(price)+"|"+
            //     DoubleToStr(opt)+"|"+DoubleToStr(tp));
         } 
         */
         
         BUModifyOk_ = true;
     }
   }
}
void closeRefreshOrder(){
   if(deleteOrder(refreshOrderId_)){
      
      refreshOrderId_ = 0;
      refreshOrderSymbol_ = "";
      //SymbolSelect(refreshOrderSymbol_, false);
        
      startPacket(CLIENT_ID, TYPE_PUSH);
      writeByte(PUSH_MSG_TYPE_REFRESH_ORDER);
      writeInt(0);
      endPacket();
   }
}
void refreshOrder(string symbol, int time){
    SymbolSelect(symbol, true);
    if (expireRefreshOrder_ < time) {
    
        expireRefreshOrder_ = time + 5;
        
        int mult= getMult(symbol);
        
        long beforeTime = nanoTime();
        bool ok;
        double price;
        double bid;
        double commission;
        
        bid = SymbolInfoDouble(symbol, SYMBOL_BID);
        if(bid == 0) return;
        if(refreshOrderId_ <= 0) {
            double minLot = SymbolInfoDouble(symbol, SYMBOL_VOLUME_MIN);
            price = bid + MathMax(1, SymbolInfoInteger(symbol, SYMBOL_SPREAD)) * 300.0 * SymbolInfoDouble(symbol, SYMBOL_POINT) * mult;
            //Print("Set refresh order ", symbol, " minlot ", minLot, " price ", price, " market price ", SymbolInfoDouble(symbol, SYMBOL_BID));
            refreshOrderId_ = sendOrder(symbol, OP_SELLLIMIT, minLot, price, 0, 0.0, 0.0, magic_, minLot, price, commission);
        } else {
            price = bid + MathMax(1, SymbolInfoInteger(symbol, SYMBOL_SPREAD)) * 300.0 * SymbolInfoDouble(symbol, SYMBOL_POINT) * mult + (50 + MathRand() / 100.0) *   SymbolInfoDouble(symbol, SYMBOL_POINT);
            //Print("Modify refresh order ", refreshOrderId_, " price ", price); 
            ok = modifyOrder(refreshOrderId_, symbol, OP_SELLLIMIT, price, 0.0, 0.0);
            if(!ok) {
               //log("Error modify refresh", refreshOrderId_);
               //Print("Error modify refresh order ", refreshOrderId_);
            }
        }
        long diffTime = round((double)(nanoTime() - beforeTime) / 1000000);
        int getlasterr = GetLastError();

        startPacket(CLIENT_ID, TYPE_PUSH);
        writeByte(PUSH_MSG_TYPE_REFRESH_ORDER);
        writeInt(diffTime);
        writeByte(refreshOrderId_ > 0 ? 1 : 0);
        endPacket();
        
        //log(LOG_UPDATE_CONNECT, IntegerToString(diffTime));
        
        //Print ("Refresh order ", refreshOrderId_ ," time ", diffTime, ", error:", getlasterr, " - ", getlasterr);
    }
}
