//+------------------------------------------------------------------+
//|                                                       etrade.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "eorder.mqh"

const int FRAME_INTERVAL = 500;
const int SLEEP_INTERVAL = 500;
const int TIMER_INTERVAL = 100;
const int EVENT_CAPACITY = 2;
const int COMMAND_CAPACITY = 4096;
const int SENDER_CAPACITY = 16384;
const int EVENT_TIMEOUT = 15000;

struct ETrade
{
public:
   ETrade();

   void onInit();
   void onDeinit();
   void onTimer();

protected:
   int getFrameId() const;
   void receiveEvent();
   void receiveCommand( int frameId );
   void updateItems();

private:
   EBuffer event_;
   EPacket command_;
   ESender * sender_;
   EOrder order_;
   
   int eventId_;
   bool isActive_;

   struct Item 
   {
   public:
      uchar type;
      double qty;
      int div;
      int profit;
   } items_[ 3 ];
};

void ETrade::ETrade() :
   event_( EVENT_CAPACITY ),
   command_( COMMAND_CAPACITY ),
   sender_( new ESender( SENDER_CAPACITY ) ),
   order_( sender_ ),
   isActive_( false )
{
}

ETrade::onInit()
{
   initNano();
   EventSetMillisecondTimer( FRAME_INTERVAL );
}

void ETrade::onDeinit() 
{
   delete sender_;
   EventKillTimer();
}

void ETrade::onTimer()
{
   int frameId = getFrameId();
   if ( frameId == 0 ) {
      return;
   }
   
   sender_.frameId = frameId;

   while ( IsStopped() == false ) {
      int delay = TIMER_INTERVAL;
      
      if ( order_.isOpen() == true ) {
         RefreshRates();

         order_.closeByTimer();
         order_.supportPos();
      
      } else if ( ( isActive_ == true ) && ( eventId_ != 0 ) && ( order_.isNew() == true ) ) {
         receiveEvent();
         delay = 0;
      } else {
         delay = SLEEP_INTERVAL;         
      }
      
      receiveCommand( frameId );
      sender_.packet.send();
      
      if ( delay > 0 ) {
         Sleep( delay );
      }
   }
}

int ETrade::getFrameId() const
{
   string comment = "";
   if ( ! ChartGetString( ChartID(), CHART_COMMENT, comment ) ) {
      Print( "Read chart comment failed" );
      return 0;
   }

   return StrToInteger( comment );
}

void ETrade::receiveEvent()
{
   if ( event_.receive( eventId_, EVENT_TIMEOUT ) == false )
      return;
      
   uchar type = event_.readByte();

   int operation = type % 2 == 0 ? OP_SELL : OP_BUY;
   
   switch ( type ) {
   case 4: case 5:
      type = 2; // test
      break;
   
   case 6: case 7: 
      type = 1; // agr
      break;
   
   case 8: case 9: 
      type = 0; // norm
      break;
   
   default:
      Print( "Invalid type ", type );
      return;
   }


   string positionId = "s" + IntegerToString( eventId_ );
   double qty = items_[ type ].qty;
   int div = items_[ type ].div;
   int profit = items_[ type ].profit;
   order_.open( positionId, operation, qty, div, div, profit );
}

void ETrade::receiveCommand( int frameId )
{
   if ( command_.receive( frameId ) == false )
      return;

   while ( command_.next() == true ) {
      switch ( command_.readByte() ) {
      case CMD_SET_EVENT:
         eventId_ = command_.readInteger();
         order_.update( command_ ); 
         updateItems();
         break;

      case CMD_SET_ACTIVE:
         isActive_ = command_.readByte() != 0;
         break;

      case CMD_CLOSE_ORDER:
         if ( true ) {
            bool force = command_.readByte() != 0;
            order_.closeByHand( force, command_.readWord() );
         }
         break;
         
      case CMD_CLOSE_ORDER_BY_PRICE:
         order_.closeByPrice( 0.000001 * command_.readLong() );
         break;
         
      case CMD_SET_ORDER:
         order_.openManual( command_.readLong() );
         break;
      }
   }
}

void ETrade::updateItems()
{
   for ( uchar i = 0, count = command_.readByte(); i < count; ++ i ) {
      uchar type = command_.readByte();
	   
	   switch ( type ) {
	   case 1: // norm
	      type = 0;
	      break;
	      
	   case 2: // agr
	      type = 1;
	      break;
	      
	   case 4: // test
	      type = 2;
	      break;
	   
	   default:
	      Print( "Unknown type: ", type );

	      command_.readInteger(); // qty
	      command_.readInteger(); // div
	      command_.readInteger(); // profit

	      continue;
      }
      
      items_[ type ].qty = 0.001 * command_.readInteger();
      items_[ type ].div = command_.readInteger();
      items_[ type ].profit = command_.readInteger();
   }
}

/***********************************************************
 * For backward compatibility
 ***********************************************************/
 
static ETrade etrade_;
 
void doOnInit()
{
   etrade_.onInit();
}
 
void doOnDeinit()
{
   etrade_.onDeinit();
}

void doOnTimer()
{
   etrade_.onTimer();
}