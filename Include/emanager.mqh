//+------------------------------------------------------------------+
//|                                                         init.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"

//+------------------------------------------------------------------+
//|                                                       etrade.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "esender.mqh"

const int TIMER_INTERVAL = 500;
const int COMMAND_CAPACITY = 4096;
const int SENDER_CAPACITY = 16384;

struct EManager
{
public:
   EManager();

   void onInit();
   void onDeinit();
   void onTimer();

private:
   void receiveCommand();
   void pushState();

private:
   class Chart
   {
   public:
      int frame;
      string symbol;
      long id;
   };

   void appendChart( Chart * chart );
   void removeChart( Chart * chart );
   
   int findChart( int frame );
   int findSymbol( string name );
   
private:
   long expire_;
   EPacket command_;
   ESender * sender_;
   Chart * charts_[];
   string symbols_[];
};

void EManager::EManager() :
   expire_( 0 ),
   command_ ( COMMAND_CAPACITY ),
   sender_( new ESender( SENDER_CAPACITY ) )
{
}

EManager::onInit()
{
   initId();
   initNano();
   sender_.frameId = CLIENT_ID;
   sender_.packet.begin( TYPE_PUSH );
   sender_.packet.writeByte( PUSH_MSG_TYPE_LOGON );
   sender_.packet.end();

   string symbols[];
   ArrayResize( symbols, SymbolsTotal( true ) );
   for ( int i = 0, symbolsCount = ArraySize( symbols ); i < symbolsCount; ++ i ) {
      symbols[ i ] = SymbolName( i, true );
   }

   for( int i = 0, symbolsCount = ArraySize( symbols ); i < symbolsCount; ++ i ) {
      SymbolSelect( symbols[ i ], false );
   }

   EventSetMillisecondTimer( TIMER_INTERVAL );
}

void EManager::onDeinit() 
{
   ArrayFree( symbols_ );
   ArrayFree( charts_ );
   delete sender_;
   EventKillTimer();
}

void EManager::onTimer()
{
   receiveCommand();
   pushState();
   
   const long time = GetTickCount();
   if ( expire_ < time ) {
      expire_ = time + 500;
      sender_.packet.send();
   }
}

void EManager::receiveCommand()
{
   if ( command_.receive( CLIENT_ID ) == false )
      return;

   while ( command_.next() == true ) {
      switch ( command_.readByte() ) {
      case CMD_PING:
         sender_.packet.begin( TYPE_PUSH );
         sender_.packet.writeByte( PUSH_MSG_TYPE_PING );
         sender_.packet.writeLong( nanoTime() );
         sender_.packet.end();
         break;

      case CMD_FRAME_START:
         if ( true ) {
            Chart * chart = new Chart();
            chart.frame = command_.readInteger();
            removeChart( chart );

            chart.symbol = command_.readString();
            appendChart( chart );
         }
         break;

      case CMD_FRAME_STOP:
         if ( true ) {
            Chart * chart = new Chart();
            chart.frame = command_.readInteger();
            removeChart( chart );
            delete chart;
         }         
         break;
      }
   }
}

void EManager::pushState()
{
   int symbolCount = ArraySize( symbols_ );
   if ( symbolCount > 255 ) {
      symbolCount = 255;
      ArrayResize( symbols_, symbolCount );
   }
   
   sender_.packet.begin( TYPE_PUSH );
   sender_.packet.writeByte( PUSH_MSG_TYPE_MARKET_DATA );
   sender_.packet.writeInteger( TimeLocal());
   sender_.packet.writeInteger( round( AccountInfoDouble( ACCOUNT_BALANCE ) * 100 ) );
   sender_.packet.writeString( getAccountCurrency() );

   sender_.packet.writeByte( symbolCount ); 
         
   for ( int i = 0; i < symbolCount; ++ i ) {
      string symbol = symbols_[ i ];

      sender_.packet.writeString( symbol );
      sender_.packet.writeByte( round( SymbolInfoInteger( symbol, SYMBOL_DIGITS ) ) );
      sender_.packet.writeByte( getMult( symbol ) );
      sender_.packet.writeLong( round( SymbolInfoDouble( symbol, SYMBOL_ASK ) * 100000 ) );
      sender_.packet.writeLong( round( SymbolInfoDouble( symbol, SYMBOL_BID ) * 100000 ) );
      sender_.packet.writeInteger( round( getMaxLot( symbol ) * 100 ) );
   }
   
   sender_.packet.end();
   
   
}

void EManager::appendChart( Chart * chart )
{
   chart.id = ChartOpen( chart.symbol, 0 );
   ChartSetString( chart.id, CHART_COMMENT, IntegerToString( chart.frame ) );
   if ( ! ChartApplyTemplate( chart.id, "etrade.tpl" ) ) {
      Print( "Template etrade.tpl not found" );
   }
   
   /* Append item to charts_ */
   int count = ArraySize( charts_ );
   ArrayResize( charts_, count + 1 );
   charts_[ count ] = chart;
   
   /* Append item to symbols_ */
   count = ArraySize( symbols_ );
   for ( int i = 0; i < count; ++ i ) {
      if ( symbols_[ i ] == chart.symbol )
         return;
   }
   ArrayResize( symbols_, count + 1 );
   symbols_[ count ] = chart.symbol;
}


void EManager::removeChart( Chart * chart )
{
   int index = findChart( chart.frame );
   if ( index == -1 )
      return;

   chart.id = charts_[ index ].id;
   chart.symbol = charts_[ index ].symbol;
   delete charts_[ index ];
   ChartClose( chart.id );
   
   /* Remove item from charts_ */
   int count = ArraySize( charts_ );
   for ( int i = index; i < count; ++ i ) {
      int next = i + 1;
      if ( next < count ) {
         charts_[ i ] = charts_[ next ];
      }
   }
   ArrayResize( charts_, count - 1 );
   
   /* Find symbol in symbols_ */
   index = findSymbol( chart.symbol );
   if ( index == -1 )
      return;

   /* Remove found symbol from symbols_ */
   count = ArraySize( symbols_ );
   for ( int i = index; i < count; ++ i ) {
      int next = i + 1;
      if ( next < count ) {
         symbols_[ i ] = symbols_[ next ];
      }
   }
   ArrayResize( symbols_, count - 1 );
}

int  EManager::findChart( int frame )
{
   int result = -1;
   
   for ( int i = 0, count = ArraySize( charts_ ); i < count; ++ i ) {
      if ( charts_[ i ].frame != frame )
         continue;
         
      result = i;
      break;
   }
   
   return result;
}


int EManager::findSymbol( string name )
{
   int result = -1;
   
   for ( int i = 0, count = ArraySize( symbols_ ); i < count; ++ i ) {
      if ( symbols_[ i ] != name )
         continue;
         
      result = i;
      break;
   }
   
   return result;
}
   
/***********************************************************
 * For backward compatibility
 ***********************************************************/
 
static EManager emanager_;
 
void doOnInit()
{
   emanager_.onInit();
}
 
void doOnDeinit()
{
   emanager_.onDeinit();
}

void doOnTimer()
{
   emanager_.onTimer();
}


  