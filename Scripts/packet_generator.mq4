//+------------------------------------------------------------------+
//|                                             packet_generator.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                     https://bitbucket.org/stansage/stansage-mql/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/stansage-mql/"
#property strict

#include "../Include/epacket.mqh"

void generateActivation( EPacket & packet, bool active ) 
{
   packet.begin( 0x04 ); // CMD_SET_ACTIVE
   packet.writeByte( active ? 1 : 0 ); // IS_ACTIVE
   packet.end();
}

void generateTimer( EPacket & packet )
{
   packet.writeInteger( 0 ); // TIMER
   packet.writeByte( 0 ); // TIMER_AUTO
   packet.writeByte( 0 ); // IS_TIMER_TP
   packet.writeByte( 0 ); // IS_TIMER_SL
}

void generateEvent( EPacket & packet, int id ) 
{
   packet.begin( CMD_SET_EVENT );
   packet.writeInteger( id ); // EVENT_ID
   packet.writeString( Symbol() ); // SYMBOL
   packet.writeWord( 4 ); // SYMBOL_MULT
   packet.writeInteger( 23 ); // STOP_LOSS
   packet.writeInteger( 11 ); // POINTS_BU
   packet.writeInteger( 7 ); // NORM_SPREAD
   packet.writeInteger( 5 ); // DIV_BU
   packet.writeByte( 0 ); // IS_REVERSE
   packet.writeInteger( 69 ); // ORDER_MAGIC
}

void generateItems( EPacket & packet, int count ) 
{
   packet.writeByte( count ); // ITEMS_COUNT
   for ( int i = 0; i < count; ++ i ) {
      switch ( i ) {
      case 0:
         packet.writeByte( 1 ); // ITEM_TYPE NORM
         break;
      case 1:
         packet.writeByte( 2 ); // ITEM_TYPE AGR
         break;
      case 2:
         packet.writeByte( 4 ); // ITEM_TYPE TEST
         break;
      }
      
      packet.writeInteger( ( i + 1 ) * 2500 ); // ITEM_QTY
      packet.writeInteger( ( i + 1 ) * 111 ); // ITEM_DIV
      packet.writeInteger( ( i + 1 ) * 10 ); // ITEM_PROFIT
   }
   packet.end();
}

void generateOrderTests() 
{
   EPacket packet( 4096 );
   generateActivation( packet, true );
   packet.send();
   
   generateEvent( packet, 101 );
   generateTimer( packet );
   generateItems( packet, 3 );
   packet.send();

   packet.begin( CMD_CLOSE_ORDER ); 
   packet.writeByte( 1 ); // FORCE
   packet.writeWord( 100 ); // PERCENT
   packet.end();
      
   generateEvent( packet, 102 );
   generateTimer( packet );
   generateItems( packet, 3 );
   
   packet.begin( CMD_CLOSE_ORDER_BY_PRICE );
   packet.writeLong( 1000 ); // PRICE
   packet.end();
   
   generateActivation( packet, false );
   packet.send();
}

void generateTimerTests() 
{
   EPacket packet( 4096 );

   generateActivation( packet, true );
   
   for ( int test = 1; test < 5; ++ test ) {
      generateEvent( packet, 200 + test );
      
      packet.writeInteger( test * 111 ); // TIMER
      packet.writeByte( 3 - ( test % 2 + 1 ) ); // TIMER_AUTO
      switch ( test ) {
      case 1:
         packet.writeByte( 0 ); // IS_TIMER_TP
         packet.writeByte( 0 ); // IS_TIMER_SL
         break;
      case 2:
         packet.writeByte( 0 ); // IS_TIMER_TP
         packet.writeByte( 1 ); // IS_TIMER_SL
         break;
      case 3:
         packet.writeByte( 1 ); // IS_TIMER_TP
         packet.writeByte( 0 ); // IS_TIMER_SL
         break;
      case 4:
         packet.writeByte( 1 ); // IS_TIMER_TP
         packet.writeByte( 1 ); // IS_TIMER_SL
         break;
      }

      generateItems( packet, 1 );
      if ( test < 4 ) {
         packet.send();
      }
   }
   
   generateActivation( packet, false );

   packet.send();
}

void generateManagerTests() 
{
   EPacket packet( 4096 );
   
   packet.begin( CMD_PING );
   packet.end();
   
   packet.begin( CMD_FRAME_START );
   packet.writeInteger( 10 );
   packet.writeString( Symbol() );
   packet.end();
   
   packet.send();
   
   packet.begin( CMD_FRAME_START );
   packet.writeInteger( 11 );
   packet.writeString( Symbol() );
   packet.end();
   
   packet.begin( CMD_FRAME_START );
   packet.writeInteger( 12 );
   packet.writeString( Symbol( ) );
   packet.end();
   
   packet.send();
   
   packet.begin( CMD_FRAME_STOP );
   packet.writeInteger( 11 );
   packet.end();
   
   packet.begin( CMD_FRAME_STOP );
   packet.writeInteger( 12 );
   packet.end();
   
   packet.send();
   
   packet.begin( CMD_FRAME_STOP );
   packet.writeInteger( 10 );
   packet.end();
   
   packet.send();
   
   
}

void OnStart()
{
   //generateOrderTests();
   //generateTimerTests();
   generateManagerTests();
}